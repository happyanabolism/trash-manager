# Trash manager #

This application for managing trashes on your computer.

On main page you can view list of trashes.
There you can create, edit, view and remove trashes.
For an individual trash you can specify maximal size of storage, path to trash in file system, name and recycle policy.
![Main page](https://bitbucket.org/happyanabolism/illustrations-for-projects/raw/master/tm_main.png)

On page with detail view of trash you can view settings, content and list of tasks with statuses of perfoming and description of errors.
There you can create task for trash.
![Trash detail](https://bitbucket.org/happyanabolism/illustrations-for-projects/raw/master/tm_trash.png)

### What can a task do? ###

* remove files, links, directories in trash storage
* restore files, links, directories from trash storage to old path
* remove files with regular expression in trash from directory
* the task can be performed with force mode, dry-run mode and symlink mode
* remove files in parallel for disjoint directories

On page with detail view of task you can view number of task, request, modes, cleaned files, status and error description if status is error.
![Task detail](https://bitbucket.org/happyanabolism/illustrations-for-projects/raw/master/tm_task.png)

### What about recycle policies? ###

* when there is not enough space in the trash to put the object it calls recycle policy
* there are 3 recycle policies: full, time, big
* full policy clean the trash in full
* time policy clean old files while there is not enough space for moving objects
* big policy clean big files while there is not enough space for moving objects

Created on python/Django.
Database: sqlite3.

### How to install? ###

* python manage.py migrate

### How to run? ###

* python manage.py runserver

Created by Nikita Batura
mail: nik.batura.97@mail.ru