# -*- coding: utf-8 -*-

from django.db import models


POLICIES = (
	(u'Full', u'Full'),
	(u'Big', u'Big'),
	(u'Time', u'Time')
)

class Trash(models.Model):
	name = models.CharField(max_length=20)
	path = models.CharField(max_length=100)
	used_space = models.FloatField(default=0)
	capacity = models.IntegerField()
	policy = models.CharField(max_length=10, choices=POLICIES, default='Full')
	created_date = models.DateTimeField(auto_now_add=True, editable=False)
	content = models.ManyToManyField('Object')

	def __str__(self):
		return self.name

	class Meta:
		ordering = ['name', 'created_date']
		unique_together = ['name', 'path']


class Task(models.Model):
	name = models.CharField(max_length=15, null=True)
	action = models.CharField(max_length=10, choices=((u'remove', u'remove'), (u'restore', u'restore')), default='remove')
	path = models.TextField(null=True)
	regex = models.BooleanField(default=False)
	force = models.BooleanField(default=False)
	symlink = models.BooleanField(default=False)
	dryrun = models.BooleanField(default=False)
	created_date = models.DateTimeField(auto_now_add=True, editable=False, null=True)
	status = models.CharField(max_length=15, editable=False, null=True)
	error_description = models.CharField(max_length=100, editable=False, null=True)
	trash = models.ForeignKey('Trash', on_delete=models.CASCADE, null=True)
	objects_for_cleaning = models.ManyToManyField('Object')

	def __str__(self):
		return self.name

	class Meta:
		ordering = ['-created_date']


class Object(models.Model):
	name = models.CharField(max_length=100, null=True)
	is_folder = models.BooleanField(default=False)
	is_file = models.BooleanField(default=False)
	is_link = models.BooleanField(default=False)

	def __str__(self):
		return self.name


