from django.contrib import admin
from .models import Trash, Task


admin.site.register(Trash)
admin.site.register(Task)

# Register your models here.
