# -*- coding: utf-8 -*-

from django import forms
from django.core.exceptions import ValidationError, ObjectDoesNotExist

from trash_list.models import Trash, Task, POLICIES
from os.path import exists

NAME_ERROR_LIST = {'min_value' : 'Error: value must be > 1',
				   'max_value' : 'Error: value must be < 100',
				   'required' : 'Error: this field is required',
				   'max_length' : 'Error: length exceed allowable value'}

HELP_TEXTS = {'name' : 'max: 30 symbols',
			  'path' : 'max: 100 symbols',
			  'capacity' : 'min: 1 mb. | max: 100 mb.'}

def validate_path(value):
	if not exists(value):
		raise ValidationError('Path \'%s\' does not exist' %value)


class TrashForm(forms.ModelForm):
	class Meta:
		model = Trash
		fields = ['name', 'path', 'capacity', 'policy']

	def clean(self):
		cleaned_data = super(TrashForm, self).clean()
		try:
			if cleaned_data['capacity'] < Trash.objects.get(name=cleaned_data['name']).used_space:
				raise ValidationError('Cannot change capacity less than used space')
		except ObjectDoesNotExist:
			pass
		return cleaned_data

	name = forms.CharField(label='Name ',
						   help_text=HELP_TEXTS['name'],
						   max_length=30,
						   required=True,
						   error_messages=NAME_ERROR_LIST)
	
	path = forms.CharField(label='Path to trash ',
						   max_length = 100, required=True,
						   help_text=HELP_TEXTS['path'],
						   error_messages=NAME_ERROR_LIST,
						   validators = [validate_path])
	
	capacity = forms.IntegerField(label='Capacity ',
								  min_value = 1,
								  max_value = 100,
								  required=True,
								  help_text=HELP_TEXTS['capacity'],
								  error_messages=NAME_ERROR_LIST)

	policy = forms.ChoiceField(label='Cleaning policy ',
							   choices=POLICIES,
							   widget = forms.RadioSelect)


class TaskForm(forms.ModelForm):
	class Meta:
		model = Task
		fields = ['action', 'path', 'regex', 'force', 'symlink', 'dryrun']

	path = forms.CharField(label='Path', max_length = 1000, required=True,
						   widget=forms.Textarea(attrs={"rows": 5, "cols": 70}),
						   error_messages=NAME_ERROR_LIST)

