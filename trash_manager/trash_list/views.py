# -*- coding: utf-8 -*-

from django.views.generic.base import ContextMixin, TemplateView
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.views.generic.edit import ProcessFormView
from django.core.urlresolvers import reverse, reverse_lazy
from django.core.paginator import Paginator, InvalidPage
from django.http import Http404
from django.shortcuts import redirect, render_to_response

from models import Trash, Task, Object
from forms import TrashForm, TaskForm
from rmv import t
from rmv import task as ta
from rmv import exceptions2
from os.path import isfile, isdir, islink, join, basename
from os import listdir


class TrashEditMixin(ContextMixin):
	def get_context_data(self, **kwargs):
		context = super(TrashEditMixin, self).get_context_data(**kwargs)
		try:
			context['pn'] = self.request.GET['page']
		except KeyError:
			context['pn'] = '1'
		return context


class TrashListView(ListView, TrashEditMixin):
	template_name = 'trash_list/trash_list.html'
	queryset = Trash.objects.order_by('name')
	paginate_by = 5


class TrashDetailView(DetailView, TrashEditMixin):
	template_name = 'trash_list/trash_detail.html'
	model = Trash
	def get_context_data(self, **kwargs):
		context = super(TrashDetailView, self).get_context_data(**kwargs)
		context['tasks'] = Task.objects.filter(trash=self.object)
		trash_path = join(kwargs['object'].path, kwargs['object'].name)
		context['content_names'] = listdir(trash_path)
		return context


class TrashEditView(ProcessFormView):
	def post(self, request, *args, **kwargs):
		try:
			pn = request.GET['page']
		except KeyError:
			pn = '1'
		self.success_url = self.success_url + '?page=' + pn
		return super(TrashEditView, self).post(request, *args, **kwargs)


class TrashCreate(CreateView, TrashEditMixin, TrashEditView):
	form_class = TrashForm
	template_name = 'trash_list/trash_create.html'
	def post(self, request, *args, **kwargs):
		self.success_url = reverse('trash_list')
		try:
			t.Trash.initialize_trash(request.POST)
		except exceptions2.SourceError as error:
			return render_to_response('trash_list/error_page_trash.html',
							{'error_description' : error, 'pn': request.GET['page']})
		return super(TrashCreate, self).post(request, *args, **kwargs)


class TrashUpdate(UpdateView, TrashEditMixin, TrashEditView):
	model = Trash
	form_class = TrashForm
	template_name = 'trash_list/trash_edit.html'
	def post(self, request, *args, **kwargs):
		self.success_url = reverse('trash_list')
		new_params = request.POST
		t.Trash.update_trash(Trash.objects.get(pk=kwargs['pk']), new_params)
		return super(TrashUpdate, self).post(request, *args, **kwargs)


class TrashDelete(DeleteView, TrashEditMixin, TrashEditView):
	model = Trash
	template_name = 'trash_list/trash_delete.html'
	def post(self, request, *args, **kwargs):
		self.success_url = reverse('trash_list')
		t.Trash.delete_trash(Trash.objects.get(pk=kwargs['pk']))
		return super(TrashDelete, self).post(request, *args, **kwargs)


class TaskCreate(TemplateView, TrashEditMixin, TrashEditView):
	form = None
	model = Task
	template_name = 'trash_list/task_create.html'
	def get(self, request, *args, **kwargs):
		self.form = TaskForm()
		return super(TaskCreate, self).get(request, *args, **kwargs)
	def get_context_data(self, **kwargs):
		context = super(TaskCreate, self).get_context_data(**kwargs)
		context['form'] = self.form
		return context
	def post(self, request, *args, **kwargs):
		self.form = TaskForm(request.POST)
		if self.form.is_valid():
			trash = Trash.objects.get(pk=kwargs['pk'])

			self.form.cleaned_data['name'] = 'task #'+str(Task.objects.count())
			self.form.cleaned_data['trash'] = trash
			
			task = Task(name=self.form.cleaned_data['name'],
						path=self.form.cleaned_data['path'],
						action=self.form.cleaned_data['action'],
						regex=self.form.cleaned_data['regex'],
						force=self.form.cleaned_data['force'],
						symlink=self.form.cleaned_data['symlink'],
						dryrun=self.form.cleaned_data['dryrun'],
						trash=self.form.cleaned_data['trash'])
			task.save()

			rmtask = ta.Task(self.form.cleaned_data,
							 trash.name,
							 trash.path,
							 trash.capacity,
							 trash.policy,
							 trash.used_space)

			status = 'completed'
			error_description = None

			for obj in rmtask.objects_for_cleaning:
				object_model = Object()
				object_model.name = obj
				object_model.is_file = isfile(obj)
				object_model.is_folder = isdir(obj)
				object_model.is_link = islink(obj)
				object_model.save()
				task.objects_for_cleaning.add(object_model)

			try: 
				rmtask.run()
			except (exceptions2.AccessError, exceptions2.SourceError,
					exceptions2.NotEnoughMemoryError, exceptions2.RestoreError) as error:
				status = 'error'
				error_description = error

			trash = Trash.objects.filter(pk=kwargs['pk'])
			trash.update(used_space=round(t.Trash.used_space(trash.first()), 2))
			task.status = status
			task.error_description = error_description
			task.save()


			if error_description != None:
				return render_to_response('trash_list/error_page.html',
							{'error_description' : error_description, 'pk': kwargs['pk'], 'pn': request.GET['page']})

			return redirect('trash_detail', pk=kwargs['pk'])
		else:
			return super(TaskCreate, self).get(request, *args, **kwargs)


class TaskDetailView(DetailView, TrashEditMixin):
	model = Task
	template_name = 'trash_list/task_detail.html'
	def get_context_data(self, **kwargs):
		context = super(TaskDetailView, self).get_context_data(**kwargs)
		context['pk'] = kwargs['object'].trash.pk
		return context
