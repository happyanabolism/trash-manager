# -*- coding: utf-8 -*-
# Generated by Django 1.9 on 2017-08-13 22:30
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('trash_list', '0005_task'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='task',
            options={'ordering': ['-created_date']},
        ),
        migrations.AddField(
            model_name='task',
            name='created_date',
            field=models.DateTimeField(auto_now_add=True, null=True),
        ),
        migrations.AddField(
            model_name='task',
            name='dryrun',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='task',
            name='force',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='task',
            name='path',
            field=models.CharField(max_length=100, null=True),
        ),
        migrations.AddField(
            model_name='task',
            name='regex',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='task',
            name='silent',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='task',
            name='symlink',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='task',
            name='trash',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='trash_list.Trash'),
        ),
        migrations.AlterField(
            model_name='task',
            name='name',
            field=models.CharField(max_length=15, null=True),
        ),
    ]
