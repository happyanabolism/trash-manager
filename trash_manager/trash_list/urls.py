from django.conf.urls import url
from views import (TrashListView, TrashDetailView,
TrashDelete, TrashUpdate, TrashCreate, TaskCreate,
TaskDetailView
)

urlpatterns = [
	url(r'^$', TrashListView.as_view(), name='trash_list'),
	url(r'^trash/(?P<pk>\w+)/$', TrashDetailView.as_view(), name='trash_detail'),
	url(r'^(?P<pk>\d+)/delete$', TrashDelete.as_view(), name='trash_delete'),
	url(r'^(?P<pk>\d+)/edit$', TrashUpdate.as_view(), name='trash_edit'),
	url(r'^new/$', TrashCreate.as_view(), name='trash_create'),
	url(r'^trash/(?P<pk>\d+)/newtask/$', TaskCreate.as_view(), name='task_create'),
	url(r'^task/(?P<pk>\d+)/$', TaskDetailView.as_view(), name='task_detail'),
]