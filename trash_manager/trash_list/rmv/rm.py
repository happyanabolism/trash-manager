#!/usr/bin/env python 
# -*- coding: utf-8 -*-

import re
from os.path import join, getsize, isdir, isfile, splitext
from os.path import dirname, exists, basename, realpath
from os import listdir, remove, walk, rmdir, access, W_OK, stat

import exceptions2	

# Alternative for shutil.rmtree
def rmtree(dir):
	names = listdir(dir)
	for name in names:
		absolute_path = join(dir, name)
		if isdir(absolute_path):
			rmtree(absolute_path)
		elif isfile(absolute_path):
			remove(absolute_path)
	rmdir(dir)


def files_regex(dir, pattern):
	if not exists(dir):
		raise exceptions2.SourceError('folder \'%s\' does not exist' %basename(dir))
	names = []
	for root, dirs, files in walk(dir):
		for file in files:
			if re.findall(pattern, file):
				names.append(join(root, file))
	return names


def dir_size(dir):
	capacity = 0.0
	for root, dirs, files in walk(dir):
		for file in files:
			capacity += getsize(join(root, file))
		for directory in dirs:
			capacity += dir_size(join(root, directory))
	return capacity / 1e6


def file_size(file):
	return getsize(file) / 1e6


def objects_size(objects):
	capacity = 0.0
	try:
		for obj in objects:
			if isdir(obj):
				capacity += dir_size(obj)
			elif isfile(obj):
				capacity += file_size(obj)
	except:
		pass
	return capacity


# Clear trash
def clean_dir(dir, exclude=()):
	for name in listdir(dir):
		if name in exclude:
			continue
		absolute_path = join(dir, name)
		if isdir(absolute_path):
			rmtree(absolute_path)
			continue
		remove(absolute_path)


def split_for_dirs(files):
	d = {}
	for file in files:
		d.setdefault(dirname(file), []).append(file)
	return d


# Changing the name of file or directory
def rename_object(oldname):
	try:
		index = re.findall(r'\[\d+\]', oldname).pop()
		index = re.findall(r'\d+', index).pop()

		newname = re.sub(r'\[\d+\]', '[' + str(int(index) + 1) + ']', oldname)
	except:
		if re.search(r'\.', oldname):
			newname = re.sub(r'\.', '[1].', oldname)
		else:
			newname = re.sub(r'$', '[1]', oldname)
	return newname


def check_objects(objects, ignore_errors, symlink):
	for obj in objects:
		if not exists(obj):
			if ignore_errors:
				objects.remove(obj)
				continue
			raise exceptions2.SourceError('object \'%s\' does not exist' %basename(obj))
		if not access(obj, W_OK):
			if ignore_errors:
				objects.remove(obj)
				continue
			raise exceptions2.AccessError('permission denied \'%s\' ' %basename(obj))
		if symlink:
			realpath = realpath(obj)
			if not exists(realpath):
				if ignore_errors:
					objects.remove(obj)
					continue
				raise exceptions2.SymlinkError('object referenced by the symlink does not exist')
	return objects




