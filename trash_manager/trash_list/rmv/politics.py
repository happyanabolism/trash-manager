# -*- coding: utf-8 -*- 

from os import walk, remove
from os.path import join, getsize, getctime, basename
from rm import clean_dir
from content import from_content


class CleaningPolicy(object):
	def __init__(self, trash, clean_space):
		self.cleaning_dir = trash.absolute_path
		self.policy = trash.policy
		self.clean_space = clean_space
		self.content_file = trash.content_file
	
	def run(self):
		cleaning_files = []
		for root, dirs, files in walk(self.cleaning_dir):
			for file in files:
				cleaning_files.append(join(root, file))

		if self.policy == 'Time':
			cleaning_files = sorted(cleaning_files, key=getctime)
		elif self.policy == 'Big':
			cleaning_files = sorted(cleaning_files, key=getsize, reverse=True)
		elif self.policy == 'Full':
			clean_dir(self.cleaning_dir, exclude=('content.json'))
			return

		size = 0.0
		for file in cleaning_files:
			if basename(file) == 'content.json':
				continue

			size += getsize(file) / 1e6
			try:
				remove(file)
				from_content(self.content_file, basename(file))
			except:
				continue
			if size >= self.clean_space:
				return