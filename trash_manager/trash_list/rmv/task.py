# -*- coding: utf-8 -*-

from os import rename
from os.path import join, dirname, basename, exists

import t
from content import from_content
from rm import files_regex, check_objects, rename_object


class Task(object):
	def __init__(self, params, trash_name, trash_path,
				 trash_capacity, trash_policy, trash_used_space):
		self.name = params['name']
		self.action = params['action']
		self.path = params['path']
		self.regex = params.get('regex', False)
		self.force = params.get('force', False)
		self.symlink = params.get('symlink', False)
		self.dryrun = params.get('dryrun', False)
		self.objects_for_cleaning = self.make_cleaning_list(trash_name, trash_path)
		self.trash = t.Trash(trash_name,
							 trash_path,
							 trash_capacity,
							 trash_policy,
							 trash_used_space)
		
	def make_cleaning_list(self, trash_name, trash_path):
		objects = self.path.split(', ')
		if self.action == 'restore':
			objects = [join(trash_path, trash_name, obj) for obj in objects]
			objects = from_content(join(trash_path, trash_name, 'content.json'),
								   objects,
								   self.force,
								   self.symlink)
			return objects
		elif self.action == 'remove':
			if self.regex:
				objects = files_regex(dirname(self.path), basename(self.path))				
			
			renamed_objects = []
			for obj in objects:
				new_obj = obj
				while exists(join(trash_path, trash_name, basename(new_obj))):
					new_obj = join(dirname(new_obj), rename_object(basename(new_obj)))
				renamed_objects.append(new_obj)
				if obj != new_obj:
					rename(obj, new_obj)
		return renamed_objects

	def run(self):
		if self.action == 'remove':
			self.objects_for_cleaning = check_objects(self.objects_for_cleaning,
													  self.force,
													  self.symlink)
		self.trash.get_task(self)