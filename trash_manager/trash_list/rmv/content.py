# -*- coding: utf-8 -*- 

from os.path import join, basename
from json import load, dump
from rm import check_objects
import exceptions2

def to_content(content_file, name, path):
	try:
		content = load(open(content_file, 'r'))
	except ValueError:
		content = {}
	content[name] = path
	dump(content, open(content_file, 'w'))	


def from_content(content_file, names, ignore_errors, symlink):
	names = check_objects(names, ignore_errors, symlink)

	try:
		content = load(open(content_file, 'r'))
	except ValueError:
		content = {}

	old_paths = []
	for name in names:
		old_paths.append(content[basename(name)])
		del content[basename(name)]
		
	dump(content, open(content_file, 'w'))
	return old_paths


