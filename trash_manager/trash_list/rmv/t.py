# -*- coding: utf-8 -*- 

from os import mkdir, rename, makedirs
from os.path import join, basename, dirname, exists
from content import to_content, from_content
from threading import Thread, Lock

import politics
import exceptions2 
from rm import (rmtree,
				files_regex,
				dir_size,
				objects_size,
				split_for_dirs,
				check_objects,
				rename_object)


class Trash(object):
	@staticmethod
	def initialize_trash(params):
		absolute_path = join(params['path'], params['name'])
		if exists(absolute_path):
			raise exceptions2.SourceError('trash with name \'%s\' exists now' %basename(absolute_path))
		mkdir(absolute_path)
		content = open(join(absolute_path, 'content.json'), 'w')
		content.close()
		
	@staticmethod
	def delete_trash(trash):
		absolute_path = join(trash.path, trash.name)
		rmtree(absolute_path)
		
	@staticmethod
	def update_trash(trash, new_params):
		old_absolute_path = join(trash.path, trash.name)
		new_absolute_path = join(new_params['path'], new_params['name'])
		if old_absolute_path != new_absolute_path:
			rename(old_absolute_path, new_absolute_path)
			
	@staticmethod
	def used_space(trash):
		absolute_path = join(trash.path, trash.name)
		return dir_size(absolute_path)

	def __init__(self, name, path, capacity, policy, used_space):
		self.name = name
		self.path = path
		self.capacity = capacity
		self.policy = policy
		self.used_space = used_space
		self.absolute_path = join(self.path, self.name)
		self.content_file = join(self.absolute_path, 'content.json')

	def get_task(self, task):
		if task.action == 'restore':
			target = self.restore_objects
		elif task.action == 'remove':
			target = self.get_objects
			self.check_capacity(objects_size(task.objects_for_cleaning))
			
		d = split_for_dirs(task.objects_for_cleaning)

		lock = Lock()
		for key in d:
			thread = Thread(target=target, args=(d[key], lock, task.dryrun))
			thread.start()

	def restore_objects(self, objects, lock, dryrun):
		with lock:
			for obj in objects:
				if not dryrun:
					self.restore_obj(obj)

	def get_objects(self, objects, lock, dryrun):
		with lock:
			for obj in objects:
				if not dryrun:
					self.get_obj(obj)

	def get_obj(self, obj):
		#replacing object
		rename(obj, join(self.absolute_path, basename(obj)))
		#add info in content file
		to_content(self.content_file, basename(obj), obj)
		
	def restore_obj(self, obj):
		#restablish the old tree of object's path
		if not exists(dirname(obj)):
			makedirs(dirname(obj))
		rename(join(self.absolute_path, basename(obj)), obj)
		
	def check_capacity(self, objects_size):
		if objects_size > self.capacity:
			raise exceptions2.NotEnoughMemoryError("object's size exceed trash's size")
		elif self.capacity - self.used_space < objects_size:
			cleaning_policy = politics.CleaningPolicy(self,
								objects_size - (self.capacity - self.used_space)).run()


