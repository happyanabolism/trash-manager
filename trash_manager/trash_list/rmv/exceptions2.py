#!/usr/bin/env python 
# -*- coding: utf-8 -*-

class ModifiedException(Exception):
	def __init__(self, error_description):
		self.error_description = error_description
	def __str__(self):
		return self.error_description

class SourceError(ModifiedException):
	pass

class NotEnoughMemoryError(ModifiedException):
	pass

class RestoreError(ModifiedException):
	pass

class AccessError(ModifiedException):
	pass

class SymlinkError(ModifiedException):
	pass